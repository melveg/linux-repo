var langs = require('./language.js');
var lang_file = {};
Object.keys(langs.translations).forEach((key,translation)=>{
 for(l in langs.supported){
  var lang = langs.supported[l].code; 
  if(!(lang_file[lang]!==null && lang_file[lang]!==undefined))
    lang_file[lang] = {};
  var lkey = key.replace(/ /g,'_');
  if(lang=='en'){
   lang_file[lang][lkey] = key;
  }
  if(langs.translations[key][lang]!==null && langs.translations[key][lang]!==undefined){
   lang_file[lang][lkey] = langs.translations[key][lang];
  }
 }
});

var fs = require('fs');
for(l in langs.supported){
 var lang = langs.supported[l].code;
 if(lang_file[lang]!==null && lang_file[lang]!==undefined){
  fs.writeFileSync('./langs/'+lang+'.json', JSON.stringify(lang_file[lang]));
 }
}
