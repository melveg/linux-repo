#!/bin/bash

#. $HOME/.bash_profile

#Checking arguments and directories
if hash ffmpeg 2>/dev/null; then
 #echo "ffmpeg exists proceed"
 while getopts f:t: option
 do
  case "${option}"
  in
  f) FROM=${OPTARG};; 
  t) TO=${OPTARG};;
  esac
 done
 #echo $FROM
 #echo $TO
 if [ ! -d "$FROM" ]; then
  #echo "from exists proceed"
 #else
  echo "Aborting from directory doesn't exist"
  exit 1
 fi
 if [ ! -d "$TO" ]; then
  #echo "to exists proceed"
 #else
  echo "Aborting 'to' directory doesn't exist"
  exit 1
 fi
else
 echo "Sorry, ffmpeg is required. Install it first"
 exit 1
fi

#Defining global variables and cleaning directory format
#FEXT means FROM EXTENSION the type of files i want to convert
FEXT="VOB"
FROM="${FROM/\/\//\/}"
TO="${TO/\/\//\/}"
TO="${TO%/}"
#TEXT means TO EXTENSION the type of files i will generate
TEXT="mp4"
TMPFILE="list.txt"

#shopt -s globstar
#for filepath in "$FROM*.csv"; do
 #filename="${filepath/.csv/}"
 #filenamenoext=${filename/$FEXT/}
 #echo "$filename"
 #echo $filenamenoext
 #ffmpeg -i $filepath -b:v 3000k  -b:a 128k "$TO/$filenamenoext.$TEXT"
 #echo "$TO/$filenamenoext.$TEXT"
#done

#List files in the source directory and use FFMPEG to convert from FTEXT files to TEXT files
cd "$FROM"
#Here you can do this with another approach.. using the -exec option in the find command for example.
find "./" -type f -name "*.$FEXT" -print0 | sort -z |
while IFS= read -r -d '' file; do
 tmpfilename="$(basename -- $file)"
 filenamenoext="${tmpfilename/.$FEXT}"
 #echo "$TO$filenamenoext.$TEXT"
 echo "Converting $tmpfilename into $TO/$filenamenoext.$TEXT ..."
 sleep 5
 ffmpeg -i "./$tmpfilename" -b:v 3000k  -b:a 128k "$TO/$filenamenoext.$TEXT" -nostdin -loglevel panic
 echo "Finishing convertion...."
 echo "file '$filenamenoext.$TEXT'" >> "$TO/$TMPFILE"
 sleep 5
done

cd "$TO"
#Concat al TEXT files generated in one big file
echo "Concat MP4 files.... Generating Big DVD.$TEXT"
sleep 5
ffmpeg -f concat -i "./$TMPFILE" -c copy "./DVD.$TEXT" -nostdin -loglevel panic

#Cleaning delete TEXT files in destiny directory and leave just the big one
echo "Cleaning..."
find "./" -type f -name "*.$TEXT" -print0 |
while IFS= read -r -d '' file; do
 filename="$(basename -- $file)"
 if [ "$filename" != "DVD.$TEXT" ]; then
  echo "Deleting $filename ..."
  sleep 5
  /bin/rm "./$filename"
 fi
done

echo "Deliting $TMPFILE ..."
/bin/rm "./$TMPFILE"
